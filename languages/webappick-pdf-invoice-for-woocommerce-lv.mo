��    )      d  ;   �      �     �  
   �     �  	   �     �  	   �     �     �  	   �     �            
   '     2     ;     @     E     M     \     a  
   m     x     �     �     �     �     �     �     �     �     �     �       	     	         *  	   .     8     >     M  �  T     <     I     V  	   m     w     �  
   �     �  	   �     �     �     �     �     �     �     �      	     		     	     "	     2	     F	     Z	     v	  !   �	     �	     �	     �	     �	  	   �	     �	     
     
     ,
     5
     B
     K
     W
     ]
     p
                                    	      !                              #                        
      "                 &   )                  (                       %          $                      '    Account Name Account No Authorized Signature BIC/Swift Bank Accounts Bank Name Billing Cost Coupon(s) Credit Note Customer Note Delivery Date Dimensions Discount Fees IBAN Invoice Invoice Number Item Net Payment Order Date Order Number Order Total Order Total With Tax Order Total Without Tax Packing Slip Payment Method Qty Quantity Refund Shipping Shipping Label Shipping Method Signature Sort Code Tax Time Slot Total Total Quantity Weight Plural-Forms: nplurals=3; plural=(n%10==0 || (n%100>=11 && n%100<=19) ? 0 : n%10==1 && n%100!=11 ? 1 : 2);
Project-Id-Version: Woo Invoice Pro
PO-Revision-Date: 2020-12-30 16:31+0600
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.4.1
X-Poedit-Basepath: ..
X-Poedit-Flags-xgettext: --add-comments=translators:
X-Poedit-WPHeader: woo-invoice-pro.php
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;_nx_noop:3c,1,2;__ngettext_noop:1,2
Last-Translator: 
Language: lv
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: *.min.js
 Konta vārds Konta numurs Apstiprināts paraksts BIC/Swift Bankas konti Bankas nosaukums Norēķini Cena Kupons(i) Klienta piezīmes Klienta piezīmes Piegādes datums Izmēri Atlaide Maksas IBAN Rēķins Rēķina numurs Produkts Neto maksājums Pasūtījuma Datums Pasūtījuma numurs Pasūtījuma kopējā summa Pasūtījums kopā ar nodokli Pasūtījums kopā bez nodokļiem Iepakojuma specifikācija Apmaksas veids Daudzums Daudzums Atmaksāt Piegāde Piegādes etiķete Pasūtīšanas veids Paraksts Kārtot kodu Nodokļi Laika slots Kopā Kopējais daudzums Svars 