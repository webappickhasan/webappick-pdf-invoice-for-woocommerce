��    )      d  ;   �      �     �  
   �     �  	   �     �  	   �     �     �  	   �     �            
   '     2     ;     @     E     M     \     a  
   m     x     �     �     �     �     �     �     �     �     �     �       	     	         *  	   .     8     >     M  �  T     �            	   0     :     F     W  	   c     m     {     �     �     �     �     �     �  
   �     �     �     	     	     0	     A	  $   O	  &   t	     �	     �	     �	     �	     �	  
   �	     �	     
  	   .
  	   8
     B
     I
     X
     _
     t
                                    	      !                              #                        
      "                 &   )                  (                       %          $                      '    Account Name Account No Authorized Signature BIC/Swift Bank Accounts Bank Name Billing Cost Coupon(s) Credit Note Customer Note Delivery Date Dimensions Discount Fees IBAN Invoice Invoice Number Item Net Payment Order Date Order Number Order Total Order Total With Tax Order Total Without Tax Packing Slip Payment Method Qty Quantity Refund Shipping Shipping Label Shipping Method Signature Sort Code Tax Time Slot Total Total Quantity Weight Plural-Forms: nplurals=1; plural=0;
Project-Id-Version: Woo Invoice Pro
PO-Revision-Date: 2020-12-30 16:18+0600
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.4.1
X-Poedit-Basepath: ..
X-Poedit-Flags-xgettext: --add-comments=translators:
X-Poedit-WPHeader: woo-invoice-pro.php
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;_nx_noop:3c,1,2;__ngettext_noop:1,2
Last-Translator: 
Language: vi
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: *.min.js
 Tên tài khoản Số tài khoản Chữ ký ủy quyền BIC/Swift Ngân hàng Tên ngân hàng thanh toán Giá cả Mã ưu đãi Ghi chú tín dụng Ghi chú khách hàng Lịch giao hàng Kích thước Giảm giá Phí IBAN Hóa đơn Số hóa đơn Sản phẩm Thanh toán ròng Thời gian đặt hàng Số đơn hàng Thành tiền Tổng đơn đặt hàng có thuế Tổng đơn hàng không tính thuế Phiếu Giao Hàng Phương thức thanh toán Số lượng Số lượng Hoàn tiền Giao hàng Hãng vận chuyển Phương thức giao hàng Chữ ký Sort code Thuế Ô Thời Gian Tổng Tổng số lượng Trọng lượng 