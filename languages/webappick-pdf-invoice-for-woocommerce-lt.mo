��    )      d  ;   �      �     �  
   �     �  	   �     �  	   �     �     �  	   �     �            
   '     2     ;     @     E     M     \     a  
   m     x     �     �     �     �     �     �     �     �     �     �       	     	         *  	   .     8     >     M  �  T     R     [     n  	   �     �     �     �     �     �     �     �     �     	     	  
   #	     .	  	   3	     =	  	   P	     Z	     l	     {	     �	  "   �	      �	     �	     �	     
     
     #
     0
     <
     N
     `
     i
  
   u
     �
     �
     �
     �
                                    	      !                              #                        
      "                 &   )                  (                       %          $                      '    Account Name Account No Authorized Signature BIC/Swift Bank Accounts Bank Name Billing Cost Coupon(s) Credit Note Customer Note Delivery Date Dimensions Discount Fees IBAN Invoice Invoice Number Item Net Payment Order Date Order Number Order Total Order Total With Tax Order Total Without Tax Packing Slip Payment Method Qty Quantity Refund Shipping Shipping Label Shipping Method Signature Sort Code Tax Time Slot Total Total Quantity Weight Plural-Forms: nplurals=3; plural=(n%10==1 && (n%100<11 || n%100>19) ? 0 : n%10>=2 && n%10<=9 && (n%100<11 || n%100>19) ? 1 : 2);
Project-Id-Version: Woo Invoice Pro
PO-Revision-Date: 2020-12-30 16:08+0600
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.4.2
X-Poedit-Basepath: ..
X-Poedit-Flags-xgettext: --add-comments=translators:
X-Poedit-WPHeader: woo-invoice-pro.php
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;_nx_noop:3c,1,2;__ngettext_noop:1,2
Last-Translator: 
Language: lt
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: *.min.js
 Gavėjas Sąskaitos numeris Įgaliotojo asmens parašas BIC/Swift Banko Sąskaitos Banko pavadinimas Registracija Kaina Kuponas(ai) Kreditinis dokumentas Pirkėjo pastaba Pristatymo data Išmatavimai Nuolaida Mokesčiai IBAN Sąskaita Sąskaitos numeris Produktas Naujas mokėjimas Užsakymo data Užsakymo numeris Galutinė užsakymo suma Užsakymas iš viso su mokesčiais Užsakyti iš viso be mokesčių Pakavimo lapelis Mokėjimo būdas Kiekis Produkto kiekis Grąžinimas Pristatymas Siuntimo etiketė Pristatymo būdas Parašas Banko kodas Mokesčiai Pristatymo langas Viso Bendras Kiekis Svoris 