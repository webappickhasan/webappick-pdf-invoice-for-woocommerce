��    6      �  I   |      �     �  
   �     �  	   �     �  	   �     �     �       	                  *     8  
   D     O     X     a     g     l     q     v     y     �     �     �     �  
   �     �     �     �     �                    #     '     0     7     F     J     S     b  	   r  	   |     �  	   �     �     �     �     �  
   �     �  �  �     |	  "   �	  .   �	  	   �	     �	     
     .
     A
     N
     [
     t
     �
  ,   �
     �
     �
  	   �
            	        )     7     <     I     \     |     �     �     �  "   �     �  .     :   ?  "   z  )   �     �     �     �     �           2     6     I  )   i     �  "   �     �     �     �  	   �          !     5  	   U     $      '   6           3           %            -   4              5                  2      1   &      
             (      /   +       	                  *          0   #                              !                           )                 .   "   ,    Account Name Account No Authorized Signature BIC/Swift Bank Accounts Bank Name Billing Category Cost Coupon(s) Credit Note Customer Note Delivery Date Description Dimensions Discount Duration Email Fees From IBAN ID Invoice Invoice Number Item Items Subtotal Net Payment Order Date Order Number Order Total Order Total With Tax Order Total Without Tax Packing Slip Payment Method Phone Qty Quantity Refund Refund Address SKU Shipping Shipping Label Shipping Method Signature Sort Code Tax Time Slot To Total Total Quantity Total Weight VAT Number Weight Project-Id-Version: Woo Invoice Pro
PO-Revision-Date: 2021-01-17 12:20+0600
Last-Translator: 
Language-Team: 
Language: hi_IN
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n==0 || n==1);
X-Generator: Poedit 2.4.2
X-Poedit-Basepath: ..
X-Poedit-Flags-xgettext: --add-comments=translators:
X-Poedit-WPHeader: woo-invoice-pro.php
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;_nx_noop:3c,1,2;__ngettext_noop:1,2
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: *.min.js
 खाते का नाम खाता क्रमांक अधिकृत हस्ताक्षर BIC/Swift बैंक खाते बैंक का नाम बिलिंग वर्ग लागत कूपन (रों) क्रेडिट नोट ग्राहक नोट डिलीवरी की तारीख विवरण आयाम छूट अवधि ईमेल फीस और से IBAN आईडी इनवॉइस बीजक संख्या मद आइटम सबटोटल कुल भुगतान दिनांक ऑर्डर संख्या कुल ऑर्डर कर के साथ कुल आदेश टैक्स के बिना कुल आदेश पैकिंग पर्ची भुगतान का तरीका फ़ोन मात्रा मात्रा धन वापसी वापसी का पता SKU शिपिंग नौवहन पर्ची शिपिंग का तरीका हस्ताक्षर क्रमबद्ध कोड कर टाइम स्लॉट Til कुल कुल मात्रा कुल वजन वैट क्रमांक भार 