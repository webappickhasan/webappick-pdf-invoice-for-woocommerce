��    6      �  I   |      �     �  
   �     �  	   �     �  	   �     �     �       	                  *     8  
   D     O     X     a     g     l     q     v     y     �     �     �     �  
   �     �     �     �     �                    #     '     0     7     F     J     S     b  	   r  	   |     �  	   �     �     �     �     �  
   �     �  �  �     �	     �	  6   �	     %
     1
     Q
     m
     |
     �
     �
  !   �
     �
     �
          +     <  "   I     l  
   r     }     �     �     �     �     �      �     �     �          #  4   9  6   n     �     �     �     �     �               -     <     M     i     �     �  
   �  #   �     �  
   �     �     
          .     $      '   6           3           %            -   4              5                  2      1   &      
             (      /   +       	                  *          0   #                              !                           )                 .   "   ,    Account Name Account No Authorized Signature BIC/Swift Bank Accounts Bank Name Billing Category Cost Coupon(s) Credit Note Customer Note Delivery Date Description Dimensions Discount Duration Email Fees From IBAN ID Invoice Invoice Number Item Items Subtotal Net Payment Order Date Order Number Order Total Order Total With Tax Order Total Without Tax Packing Slip Payment Method Phone Qty Quantity Refund Refund Address SKU Shipping Shipping Label Shipping Method Signature Sort Code Tax Time Slot To Total Total Quantity Total Weight VAT Number Weight Project-Id-Version: Woo Invoice Pro
PO-Revision-Date: 2021-01-17 13:14+0600
Last-Translator: 
Language-Team: 
Language: ru
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<12 || n%100>14) ? 1 : 2);
X-Generator: Poedit 2.4.2
X-Poedit-Basepath: ..
X-Poedit-Flags-xgettext: --add-comments=translators:
X-Poedit-WPHeader: woo-invoice-pro.php
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;_nx_noop:3c,1,2;__ngettext_noop:1,2
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: *.min.js
 Название счета Номер счета Подпись Уполномоченного Лица BIC / Swift Банковские Счета Название банка Биллинг Категория Стоимость Купон(ы) Кредитная записка Заметка клиента Дата доставки Описание объекта Габариты Скидка Продолжительность Email Сборы От IBAN ID Счет Номер счёта Позиция Итого по позициям Чистая выплата Дата заказа Номер заказа Итог заказа Общая сумма заказа с налогом Общая сумма заказа без налога Накладная Способ оплаты Телефон Кол-во Количество Возврат Адрес возврата Артикул Доставка Почтовый бланк Метод доставки Подпись БИК (sort code) НАЛОГ Временной интервал Кому Итого Общее количество Общий вес Номер НДС Вес 