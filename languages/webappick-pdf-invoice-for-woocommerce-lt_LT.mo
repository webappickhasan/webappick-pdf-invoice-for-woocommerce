��    %      D  5   l      @     A  	   N     X  	   f     p     x  	   }     �     �     �  
   �     �     �     �     �     �     �     �  
   �                     &     5     9     B     I     R     a  	   q  	   {     �  	   �     �     �     �     �     �  	   �     �     �     �                    )     :     J     S  
   \     g     l          �     �     �     �     �     �     �     �     	     	     	     !	     3	     E	     N	  
   Z	     e	     w	     �	     �	                                                                           "   $                      
          #             	         !                    %                           Account Name BIC/Swift Bank Accounts Bank Name Billing Cost Coupon(s) Credit Note Customer Note Delivery Date Dimensions Discount Fees IBAN Invoice Invoice Number Item Net Payment Order Date Order Number Order Total Packing Slip Payment Method Qty Quantity Refund Shipping Shipping Label Shipping Method Signature Sort Code Tax Time Slot Total Total Quantity Weight Plural-Forms: nplurals=3; plural=(n%10==1 && (n%100<11 || n%100>19) ? 0 : n%10>=2 && n%10<=9 && (n%100<11 || n%100>19) ? 1 : 2);
Project-Id-Version: Woo Invoice Pro
PO-Revision-Date: 2021-01-06 10:05+0600
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.4.1
X-Poedit-Basepath: ..
X-Poedit-Flags-xgettext: --add-comments=translators:
X-Poedit-WPHeader: woo-invoice-pro.php
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;_nx_noop:3c,1,2;__ngettext_noop:1,2
Last-Translator: 
Language: lt_LT
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: *.min.js
 Sąskaitos pavadinimas BIC/Swift Banko Sąskaitos Banko pavadinimas Registracija Kaina Kuponas(ai) Kreditinis dokumentas Pirkėjo pastaba Pristatymo data Matmenys Nuolaida Mokesčiai IBAN Sąskaita faktūra Sąskaitos numeris Prekė Naujas mokėjimas Užsakymo data Užsakymo Numeris Užsakymo suma Pakavimo lapelis Mokėjimo būdas Vnt Kiekis Grąžinimas Pristatymas Siuntimo etiketė Pristatymo būdas Parašas Banko kodas Mokesčiai Pristatymo langas Iš viso Bendras Kiekis Svoris 