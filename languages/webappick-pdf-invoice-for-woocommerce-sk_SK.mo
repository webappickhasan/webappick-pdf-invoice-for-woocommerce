��    +      t  ;   �      �     �  
   �     �  	   �     �  	   �            	             +     9  
   G     R     [     `     e     m     |     �  
   �     �     �     �     �     �     �     �     �                    &     5  	   E  	   O     Y  	   ]     g     m     |     �  �  �     T     b     q     �     �     �     �     �  	   �     �     �     �     �      	     	     	     	     	     /	     7	     F	     Y	     m	  "   �	  "   �	     �	     �	  	   �	  	   �	     �	     
     
     %
     8
     H
  
   O
     Z
     _
  
   n
     y
     �
  	   �
                                    	      "             !                $                        
      #                 '   +                  )                       &   *      %                      (    Account Name Account No Authorized Signature BIC/Swift Bank Accounts Bank Name Billing Cost Coupon(s) Credit Note Customer Note Delivery Date Dimensions Discount Fees IBAN Invoice Invoice Number Item Net Payment Order Date Order Number Order Total Order Total With Tax Order Total Without Tax Packing Slip Payment Method Qty Quantity Refund Refund Address Shipping Shipping Label Shipping Method Signature Sort Code Tax Time Slot Total Total Quantity Total Weight Weight Project-Id-Version: Woo Invoice Pro
PO-Revision-Date: 2021-01-11 12:39+0600
Last-Translator: 
Language-Team: 
Language: sk_SK
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n>=2 && n<=4 ? 1 : 2);
X-Generator: Poedit 2.4.2
X-Poedit-Basepath: ..
X-Poedit-Flags-xgettext: --add-comments=translators:
X-Poedit-WPHeader: woo-invoice-pro.php
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;_nx_noop:3c,1,2;__ngettext_noop:1,2
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: *.min.js
 Názov účtu Číslo účtu Autorizovaný podpis BIC / Swift Bankové účty Názov banky Fakturácia Cena Kupón(y) Dobropis Poznámky zákazníka Dátum dodania Rozmery Zľava Poplatky IBAN Faktúra Číslo faktúry Produkt Čistá platba Dátum objednávky Číslo objednávky Objednávka celkom Celková suma objednávky s daňou Celková suma objednávky bez dane Dodací list Spôsob platby Počet ks Množstvo Refundácia Adresa na vrátenie platby Doprava Dopravný štítok Spôsob dopravy Podpis Kód banky DAŇ Časový úsek Cena spolu Celkové množstvo Celková váha Hmotnosť 