��    +      t  ;   �      �     �  
   �     �  	   �     �  	   �            	             +     9  
   G     R     [     `     e     m     |     �  
   �     �     �     �     �     �     �     �     �                    &     5  	   E  	   O     Y  	   ]     g     m     |     �  �  �     8     F     W     k     w     �     �     �  	   �     �     �     �     �     �     �      	     	     	      	  	   '	     1	     @	     R	     b	     }	     �	     �	     �	     �	     �	     �	     �	     �	     	
     
     $
     >
     F
     Y
     _
  
   o
     z
                                    	      "             !                $                        
      #                 '   +                  )                       &   *      %                      (    Account Name Account No Authorized Signature BIC/Swift Bank Accounts Bank Name Billing Cost Coupon(s) Credit Note Customer Note Delivery Date Dimensions Discount Fees IBAN Invoice Invoice Number Item Net Payment Order Date Order Number Order Total Order Total With Tax Order Total Without Tax Packing Slip Payment Method Qty Quantity Refund Refund Address Shipping Shipping Label Shipping Method Signature Sort Code Tax Time Slot Total Total Quantity Total Weight Weight Project-Id-Version: Woo Invoice Pro
PO-Revision-Date: 2021-01-11 13:24+0600
Last-Translator: 
Language-Team: 
Language: gl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 2.4.2
X-Poedit-Basepath: ..
X-Poedit-Flags-xgettext: --add-comments=translators:
X-Poedit-WPHeader: woo-invoice-pro.php
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;_nx_noop:3c,1,2;__ngettext_noop:1,2
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: *.min.js
 Nome da conta Número de conta Sinatura autorizada BIC / Swift Contas bancarias Nome do banco Facturacción Custo Cupón(s) Nota de crédito Nota do cliente Data de entrega Dimensións Desconto Cotas IBAN Factura Número de factura Artigo Pago neto Data de pedido Número de pedido Total do pedido Total da orde con impostos Total da orde sen impostos Resbalón de embalaxe Forma de pagamento Cantidad Cantidad Abonar Enderezo de reembolso Envio Etiqueta de envío Método de envío Sinatura Código de clasificación Imposto Intervalo de tempo Total Cantidade total Peso total Peso 