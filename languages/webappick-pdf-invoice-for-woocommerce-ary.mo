��    6      �  I   |      �     �  
   �     �  	   �     �  	   �     �     �       	                  *     8  
   D     O     X     a     g     l     q     v     y     �     �     �     �  
   �     �     �     �     �                    #     '     0     7     F     J     S     b  	   r  	   |     �  	   �     �     �     �     �  
   �     �  S  �     
     3
     G
     e
     }
     �
     �
     �
     �
     �
     �
     �
       
   4     ?     N  
   ]     h     w     �     �     �     �     �     �     �     �     �             +   6  +   b     �     �     �     �     �     �     �       
        )     ?     U     d     z     �     �     �     �     �     �  
   �     $      '   6           3           %            -   4              5                  2      1   &      
             (      /   +       	                  *          0   #                              !                           )                 .   "   ,    Account Name Account No Authorized Signature BIC/Swift Bank Accounts Bank Name Billing Category Cost Coupon(s) Credit Note Customer Note Delivery Date Description Dimensions Discount Duration Email Fees From IBAN ID Invoice Invoice Number Item Items Subtotal Net Payment Order Date Order Number Order Total Order Total With Tax Order Total Without Tax Packing Slip Payment Method Phone Qty Quantity Refund Refund Address SKU Shipping Shipping Label Shipping Method Signature Sort Code Tax Time Slot To Total Total Quantity Total Weight VAT Number Weight Project-Id-Version: Woo Invoice Pro
PO-Revision-Date: 2021-01-18 16:11+0600
Last-Translator: 
Language-Team: 
Language: ar
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language-Source: en-US
Language-Target: ar
X-Generator: Poedit 2.4.2
X-Poedit-Basepath: ..
Plural-Forms: nplurals=6; plural=(n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 && n%100<=10 ? 3 : n%100>=11 && n%100<=99 ? 4 : 5);
X-Poedit-Flags-xgettext: --add-comments=translators:
X-Poedit-WPHeader: woo-invoice-pro.php
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;_nx_noop:3c,1,2;__ngettext_noop:1,2
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: *.min.js
X-Poedit-SearchPathExcluded-1: node_modules
 أسم الحساب رقم الحساب التوقيع المعتمد رمز BIC / سويفت حساب البنك اسم البنك الفواتير التصنيف التكلفة قسيمة (قسائم) اشعار دائن ملاحظة العملاء تاريخ التسليم الوصف الأبعاد التخفيض المدة الايميل الرسوم من IBAN ‫ID فاتورة رقم الفاتورة العنصر مجموع المنتجات الدفع الصافي حالة الطلب رقم الطلب الطلب الكلي إجمالي الطلب مع الضرائب إجمالي الطلب بدون ضريبة قسيمة الشحن طريقة الدفع الهاتف الكمية الكمية إعادة المبلغ عنوان الاسترداد ‫SKU الشحن قسيمة الشحن طريقة الشحن التوقيع ترتيب الكود الضرائب مدة زمنية إلى الإجمال الكمية الإجمالية الوزن الكلي ظريبه الشراء الوزن 