��    3      �  G   L      h     i  
   v     �  	   �     �  	   �     �     �     �  	   �     �     �     �        
                   )     /     4     9     >     F     U     Z  
   f     q     ~     �     �     �     �     �     �     �     �     �     �            	   $  	   .     8  	   <     F     I     O     ^  
   k     v  �  }     (	  	   <	     F	  	   W	     a	     s	     �	  
   �	     �	  
   �	     �	     �	     �	     �	     �	  	   �	  	   
     
  
   
     
     "
     '
     /
  	   B
  	   L
     V
     g
     y
     �
     �
     �
     �
  	   �
     �
     �
  	             (     /     B     T     Z  	   t     ~     �     �     �  
   �     �     �     
   $      )                   3   	                            #          +      *             1      ,           %   &   .                  '          0   2          !   (                         /          -   "                                       Account Name Account No Authorized Signature BIC/Swift Bank Accounts Bank Name Billing Category Cost Coupon(s) Credit Note Customer Note Delivery Date Description Dimensions Discount Duration Email Fees From IBAN Invoice Invoice Number Item Net Payment Order Date Order Number Order Total Order Total With Tax Order Total Without Tax Packing Slip Payment Method Phone Qty Quantity Refund Refund Address Shipping Shipping Label Shipping Method Signature Sort Code Tax Time Slot To Total Total Quantity Total Weight VAT Number Weight Plural-Forms: nplurals=2; plural=(n != 1);
Project-Id-Version: Woo Invoice Pro
PO-Revision-Date: 2021-01-12 14:41+0600
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.4.2
X-Poedit-Basepath: ..
X-Poedit-Flags-xgettext: --add-comments=translators:
X-Poedit-WPHeader: woo-invoice-pro.php
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;_nx_noop:3c,1,2;__ngettext_noop:1,2
Last-Translator: 
Language: es_PR
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: *.min.js
 Nombre de la cuenta Cuenta No Firma autorizada BIC/SWIFT Cuentas Bancarias Nombre del banco Facturación Categoría Costo Cupón(es) Nota de Crédito Nota de compra Fecha de Entrega Descripción Dimensiones Descuento Duración Email Comisiones Desde IBAN Factura Número de factura Artículo Pago neto Fecha del pedido Número de pedido Total del pedido Pedido total sin impuestos Total del pedido sin impuestos Detalle de la entrega Método de Pago Teléfono Cantidad Cantidad Reembolso Dirección de reembolso Envío Etiqueta de envío Método de envío Firma Código de clasificación Impuestos Espacio de Tiempo Hasta Total Cantidad total Peso total Número de IVA Peso 