��    )      d  ;   �      �     �  
   �     �  	   �     �  	   �     �     �  	   �     �            
   '     2     ;     @     E     M     \     a  
   m     x     �     �     �     �     �     �     �     �     �     �       	     	         *  	   .     8     >     M  �  T     �               7     D     W     c     k  	   q     {     �     �     �     �     �     �     �     �     �     �     �     	     	     )	     C	     ]	     l	     {	     �	  
   �	  
   �	     �	     �	     �	     �	     �	     �	     �	     �	     �	                                    	      !                              #                        
      "                 &   )                  (                       %          $                      '    Account Name Account No Authorized Signature BIC/Swift Bank Accounts Bank Name Billing Cost Coupon(s) Credit Note Customer Note Delivery Date Dimensions Discount Fees IBAN Invoice Invoice Number Item Net Payment Order Date Order Number Order Total Order Total With Tax Order Total Without Tax Packing Slip Payment Method Qty Quantity Refund Shipping Shipping Label Shipping Method Signature Sort Code Tax Time Slot Total Total Quantity Weight Plural-Forms: nplurals=2; plural=(n != 1);
Project-Id-Version: Woo Invoice Pro
PO-Revision-Date: 2020-12-29 15:21+0600
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.4.2
X-Poedit-Basepath: ..
X-Poedit-Flags-xgettext: --add-comments=translators:
X-Poedit-WPHeader: woo-invoice-pro.php
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;_nx_noop:3c,1,2;__ngettext_noop:1,2
Last-Translator: 
Language: sq
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: *.min.js
 Emri i llogarisë Numër llogarie Nënshkrimi i autorizuar BIC / Shpejt Llogaritw Bankare  Emër banke Faturim Kosto Kupon(ë) Shënim krediti Shënim klienti Data e Dorëzimit Përmasa Zbritje Tarifat IBAN Fatura Numri i faturës Artikull Pagesa neto Numër Porosie Numër Porosie Totali i porosisë Porosit Totalin me Taksë Porosit Totalin pa Taksë Dosje paketimi Metodë Pagese Sasia Sasia Rimbursimi Transporti Etiketë Dërgimi Metodë Dërgimi Nënshkrimi Kod Sort Tatimi Koha filloi Total Sasia totale Peshë 