��    +      t  ;   �      �     �  
   �     �  	   �     �  	   �            	             +     9  
   G     R     [     `     e     m     |     �  
   �     �     �     �     �     �     �     �     �                    &     5  	   E  	   O     Y  	   ]     g     m     |     �  �  �  2   �  2   �  	   �  	   �     �     		     	     ,	     =	     L	     j	     �	     �	     �	  
   �	     �	     �	  (   �	     
     (
     B
     `
  %   �
  1   �
  5   �
          ,     F     Y     l  .   �     �     �     �     �               !     A     N     a     {                                    	      "             !                $                        
      #                 '   +                  )                       &   *      %                      (    Account Name Account No Authorized Signature BIC/Swift Bank Accounts Bank Name Billing Cost Coupon(s) Credit Note Customer Note Delivery Date Dimensions Discount Fees IBAN Invoice Invoice Number Item Net Payment Order Date Order Number Order Total Order Total With Tax Order Total Without Tax Packing Slip Payment Method Qty Quantity Refund Refund Address Shipping Shipping Label Shipping Method Signature Sort Code Tax Time Slot Total Total Quantity Total Weight Weight Project-Id-Version: Woo Invoice Pro
PO-Revision-Date: 2021-01-11 15:18+0600
Last-Translator: 
Language-Team: 
Language: uk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<12 || n%100>14) ? 1 : 2);
X-Generator: Poedit 2.4.2
X-Poedit-Basepath: ..
X-Poedit-Flags-xgettext: --add-comments=translators:
X-Poedit-WPHeader: woo-invoice-pro.php
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;_nx_noop:3c,1,2;__ngettext_noop:1,2
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: *.min.js
 Назва банківського рахунку Номер банківського рахунку Signature BIC/Swift Bank accounts Назва банку оплата Вартість Купон(и) Кредитове авізо Примітка покупця Дата доставки Габарити Знижка Збори IBAN Рахунок-фактура Номер рахунка-фактури Позиція Чистий платіж Дата замовлення Номер замовлення Підсумок замовлення Замовити разом із податком Замовити загалом без податку Список упаковки Спосіб оплати Кількість Кількість Повернення Адреса повернення коштів Доставка Мітка доставки Метод доставки Підпис Sort код Податок Часовий Інтервал Всього Кількість Загальна вага Вага 