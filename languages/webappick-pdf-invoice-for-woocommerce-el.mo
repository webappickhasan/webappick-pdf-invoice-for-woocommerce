��    6      �  I   |      �     �  
   �     �  	   �     �  	   �     �     �       	                  *     8  
   D     O     X     a     g     l     q     v     y     �     �     �     �  
   �     �     �     �     �                    #     '     0     7     F     J     S     b  	   r  	   |     �  	   �     �     �     �     �  
   �     �  �  �  !   s	  %   �	  /   �	     �	  +   �	     #
     ?
     L
     _
     l
  #   
     �
  '   �
     �
     �
                1     7     H     O     T     W  #   j     �  ,   �     �  +   �  %     #   6  1   Z  7   �  #   �     �               *  #   ;  8   _     �     �  !   �     �     �                     2     ;     H     Y     u  
   �     $      '   6           3           %            -   4              5                  2      1   &      
             (      /   +       	                  *          0   #                              !                           )                 .   "   ,    Account Name Account No Authorized Signature BIC/Swift Bank Accounts Bank Name Billing Category Cost Coupon(s) Credit Note Customer Note Delivery Date Description Dimensions Discount Duration Email Fees From IBAN ID Invoice Invoice Number Item Items Subtotal Net Payment Order Date Order Number Order Total Order Total With Tax Order Total Without Tax Packing Slip Payment Method Phone Qty Quantity Refund Refund Address SKU Shipping Shipping Label Shipping Method Signature Sort Code Tax Time Slot To Total Total Quantity Total Weight VAT Number Weight Project-Id-Version: Woo Invoice Pro
PO-Revision-Date: 2021-01-17 12:19+0600
Last-Translator: 
Language-Team: 
Language: el
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 2.4.2
X-Poedit-Basepath: ..
X-Poedit-Flags-xgettext: --add-comments=translators:
X-Poedit-WPHeader: woo-invoice-pro.php
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;_nx_noop:3c,1,2;__ngettext_noop:1,2
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: *.min.js
 Όνομα Λογαριασμού Αριθμός λογαριασμού Εξουσιοδοτημένη υπογραφή BIC / Swift Τραπεζικοί λογαριασμοί Όνομα Τράπεζας Χρέωση Κατηγορία Κόστος Κουπόνι(α) Πιστωτικό σημείωμα Σημείωση πελάτη Ημερομηνία παράδοσης Περιγραφή Διαστάσεις Έκπτωση Διάρκεια Email Χρεώσεις Από IBAN ID Τιμολόγιο Αριθμός Τιμολογίου Προϊόν Μερικό σύνολο στοιχείων Καθαρή πληρωμή Ημερομηνία παραγγελίας Αριθμός παραγγελίας Σύνολο παραγγελίας Σύνολο παραγγελίας με φόρο Σύνολο παραγγελίας χωρίς φόρο Δελτίο συσκευασίας Μέθοδος πληρωμής Τηλέφωνο Ποσότητα Ποσότητα Επιστροφή χρημάτων Διεύθυνση επιστροφής χρημάτων SKU Αποστολή Ετικέτα Αποστολής Τρόπος Αποστολής Υπογραφή Κωδικός sort ΦΠΑ Χρονοθυρίδα Προς Σύνολο Ποσότητα Συνολικό βάρος Αριθμός ΑΦΜ Βάρος 