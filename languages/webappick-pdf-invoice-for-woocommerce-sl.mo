��    )      d  ;   �      �     �  
   �     �  	   �     �  	   �     �     �  	   �     �            
   '     2     ;     @     E     M     \     a  
   m     x     �     �     �     �     �     �     �     �     �     �       	     	         *  	   .     8     >     M  �  T     0     >     P  	   e     o          �     �     �     �     �     �  	   �     �     �     �     �     �     	     	     	     )	     =	     D	     \	  	   x	     �	  	   �	  	   �	     �	     �	     �	     �	     �	     �	     �	     
     
     
     )
                                    	      !                              #                        
      "                 &   )                  (                       %          $                      '    Account Name Account No Authorized Signature BIC/Swift Bank Accounts Bank Name Billing Cost Coupon(s) Credit Note Customer Note Delivery Date Dimensions Discount Fees IBAN Invoice Invoice Number Item Net Payment Order Date Order Number Order Total Order Total With Tax Order Total Without Tax Packing Slip Payment Method Qty Quantity Refund Shipping Shipping Label Shipping Method Signature Sort Code Tax Time Slot Total Total Quantity Weight Plural-Forms: nplurals=4; plural=(n%100==1 ? 0 : n%100==2 ? 1 : n%100>=3 && n%100<=4 ? 2 : 3);
Project-Id-Version: Woo Invoice Pro
PO-Revision-Date: 2020-12-30 14:58+0600
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.4.1
X-Poedit-Basepath: ..
X-Poedit-Flags-xgettext: --add-comments=translators:
X-Poedit-WPHeader: woo-invoice-pro.php
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;_nx_noop:3c,1,2;__ngettext_noop:1,2
Last-Translator: 
Language: sl
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: *.min.js
 Naziv računa Številka računa Pooblaščeni podpis BIC/Swift Bančni Računi Naziv banke plačilo Cena Kupon(i) Dobropis Obvestila za stranko Datum dostave Dimenzije Popust Stroški IBAN Račun Št. naročila Izdelek Neto plačilo Datum naročila Številka naročila Skupaj Naroči skupaj z davkom Naročilo skupaj brez davka Pakiranje Način plačila Količina Količina Vračilo denarja Dostava Dostavna etiketa Način dostave Podpis Koda za razvrščanje Davek Časovna reža Znesek Skupna količina Teža 