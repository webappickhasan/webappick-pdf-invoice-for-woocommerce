��    +      t  ;   �      �     �  
   �     �  	   �     �  	   �            	             +     9  
   G     R     [     `     e     m     |     �  
   �     �     �     �     �     �     �     �     �                    &     5  	   E  	   O     Y  	   ]     g     m     |     �  �  �  	   x     �     �     �     �     �  	   �     �     �     �     �     	  
   	     &	     /	     4	  
   9	     D	     Q	     X	     e	     t	     �	     �	     �	     �	     �	  	   �	  	   �	      
     
     
     $
     9
  	   L
     V
     f
     j
     {
     �
     �
     �
                                    	      "             !                $                        
      #                 '   +                  )                       &   *      %                      (    Account Name Account No Authorized Signature BIC/Swift Bank Accounts Bank Name Billing Cost Coupon(s) Credit Note Customer Note Delivery Date Dimensions Discount Fees IBAN Invoice Invoice Number Item Net Payment Order Date Order Number Order Total Order Total With Tax Order Total Without Tax Packing Slip Payment Method Qty Quantity Refund Refund Address Shipping Shipping Label Shipping Method Signature Sort Code Tax Time Slot Total Total Quantity Total Weight Weight Project-Id-Version: Woo Invoice Pro
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2021-01-10 18:39+0600
Last-Translator: 
Language-Team: Română
Language: ro_RO
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 2.4.2
X-Poedit-Basepath: ..
X-Poedit-Flags-xgettext: --add-comments=translators:
X-Poedit-WPHeader: woo-invoice-pro.php
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;_nx_noop:3c,1,2;__ngettext_noop:1,2
X-Loco-Version: 2.3.3; wp-5.4
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: *.min.js
 Nume cont Număr cont Semnatura autorizata BIC / Swift Cont bancar Nume bancă Facturare Cost Cupon (cupoane) Nota de credit Notă pentru clienți Data de livrare Dimensiuni Discount Taxe IBAN Solicitare Nr. proforma Produs Plată netă Dată comandă Număr comandă Total Order Comanda totală cu impozite Comanda totală fără taxe Eticheta ambalare Metodă de plată Cantitate Cantitate Refund Adresa de rambursare Livrare Etichetă de livrare Metodă de livrare Semnatura Sortează codul TVA Interval de timp Total Valoare totală Greutatea totală; Greutate 