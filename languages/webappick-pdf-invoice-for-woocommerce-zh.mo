��    6      �  I   |      �     �  
   �     �  	   �     �  	   �     �     �       	                  *     8  
   D     O     X     a     g     l     q     v     y     �     �     �     �  
   �     �     �     �     �                    #     '     0     7     F     J     S     b  	   r  	   |     �  	   �     �     �     �     �  
   �     �  �  �  	   l	     v	     �	  	   �	     �	     �	     �	     �	     �	  	   �	     �	     �	     �	     �	     
     
     
     
     )
     0
     4
     9
     <
     C
     P
     W
  	   d
     n
  	   {
     �
     �
     �
     �
     �
     �
     �
     �
     �
     �
     �
          	          #     *     7  	   ;     E     I  	   P  	   Z     d     q     $      '   6           3           %            -   4              5                  2      1   &      
             (      /   +       	                  *          0   #                              !                           )                 .   "   ,    Account Name Account No Authorized Signature BIC/Swift Bank Accounts Bank Name Billing Category Cost Coupon(s) Credit Note Customer Note Delivery Date Description Dimensions Discount Duration Email Fees From IBAN ID Invoice Invoice Number Item Items Subtotal Net Payment Order Date Order Number Order Total Order Total With Tax Order Total Without Tax Packing Slip Payment Method Phone Qty Quantity Refund Refund Address SKU Shipping Shipping Label Shipping Method Signature Sort Code Tax Time Slot To Total Total Quantity Total Weight VAT Number Weight Plural-Forms: nplurals=1; plural=0;
Project-Id-Version: Woo Invoice Pro
PO-Revision-Date: 2021-01-17 12:28+0600
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.4.2
X-Poedit-Basepath: ..
X-Poedit-Flags-xgettext: --add-comments=translators:
X-Poedit-WPHeader: woo-invoice-pro.php
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;_nx_noop:3c,1,2;__ngettext_noop:1,2
Last-Translator: 
Language: zh
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: *.min.js
 帐户名 帳戶號碼 授权签名 BIC/Swift 银行账号 銀行名稱 账单 類別 成本 折價券 红字发票 客户备注 交货日期 描述 尺寸 折扣 持續時間 電子郵件 费用 從 IBAN ID 发票 发票号码 項目 項目小計 净付款 订单日期 订单号 合计订单 含税的订单总额 不含税的订单总额 清单 支付方式 電話 数量 数量 退款 退款地址 SKU 配送 货运标签 配送方式 签名 分類代碼 税 时间段 到 总计 总数量 總重量 統一編號 重量 