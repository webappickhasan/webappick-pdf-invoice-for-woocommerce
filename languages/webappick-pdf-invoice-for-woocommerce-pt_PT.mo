��    6      �  I   |      �     �  
   �     �  	   �     �  	   �     �     �       	                  *     8  
   D     O     X     a     g     l     q     v     y     �     �     �     �  
   �     �     �     �     �                    #     '     0     7     F     J     S     b  	   r  	   |     �  	   �     �     �     �     �  
   �     �  �  �     r	     �	     �	  	   �	     �	     �	  
   �	  	   �	     �	     �	     �	     

     
     *
  
   6
     A
  	   J
     T
     Z
     `
     c
     h
     k
  
   r
     }
     �
     �
     �
     �
     �
     �
     �
          -     B     K  
   O  	   Z     d     {          �     �  
   �     �     �     �     �     �     �  
   �     	          $      '   6           3           %            -   4              5                  2      1   &      
             (      /   +       	                  *          0   #                              !                           )                 .   "   ,    Account Name Account No Authorized Signature BIC/Swift Bank Accounts Bank Name Billing Category Cost Coupon(s) Credit Note Customer Note Delivery Date Description Dimensions Discount Duration Email Fees From IBAN ID Invoice Invoice Number Item Items Subtotal Net Payment Order Date Order Number Order Total Order Total With Tax Order Total Without Tax Packing Slip Payment Method Phone Qty Quantity Refund Refund Address SKU Shipping Shipping Label Shipping Method Signature Sort Code Tax Time Slot To Total Total Quantity Total Weight VAT Number Weight Project-Id-Version: Woo Invoice Pro
PO-Revision-Date: 2021-01-17 12:18+0600
Last-Translator: 
Language-Team: 
Language: pt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Poedit 2.4.2
X-Poedit-Basepath: ..
X-Poedit-Flags-xgettext: --add-comments=translators:
X-Poedit-WPHeader: woo-invoice-pro.php
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;_nx_noop:3c,1,2;__ngettext_noop:1,2
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: *.min.js
 Nome da conta Número da conta Assinatura autorizada BIC/Swift Contas Bancárias Nome do banco facturacao Categoria Custo Cupão(ões) Nota de Crédito Nota do Cliente Data de entrega Descrição Dimensões Desconto Duração Email Taxas De IBAN ID Fatura Fatura Nº Item Subtotal de itens Pagamento Líquido Data da encomenda Número da encomenda Total do Pedido Total do pedido com imposto Total do pedido sem impostos Etiqueta de Embalagem Método de pagamento telefone Qtd Quantidade Reembolso Endereço de Reembolso SKU Envio Etiqueta de envio Método de envio Assinatura Código da agência Imposto Intervalo de Tempo Para Total Quantidade total Peso Total Número de IVA Peso 