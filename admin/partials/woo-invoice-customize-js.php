<script>

    (function ( $ ) {
        'use strict';
        $( function() {
            $( "#customizable_shipping_content ul").sortable();
            $( "#cz_product_content ul").sortable();
            $(".product-table").dragtable({dragHandle: ".handle"});
            $( ".cz_order_total ul").sortable();
            // Submit Customizable shipping condent.
            $(document).on('click', '#wpifw_submit_customizable_shipping_template', function(event) {
                event.preventDefault();
                var order_data = $('#customizable_shipping_content ul li#customizable_order_data').index();
                var address = $('#customizable_shipping_content ul li#customizable_address').index()
                var seller = $('#customizable_shipping_content ul li#customizable_seller').index()
                var customize_form = $('#wpifw_customizable_form').serialize();

                // Product Header index.
                var product_header_val = [];
                var product_list_header = $('.product-list-header').children();
                for(var i = 0; i< product_list_header.length; i++){
                    var classes = product_list_header[i].getAttribute('class')
                    var first_class = classes.split(' ')
                    product_header_val[i] = first_class[0];
                }
                product_header_val =  JSON.stringify(product_header_val)
                // Product Body index.
                var product_body = [];
                var product_list = $('.product-list').children();
                for(var i = 0; i< product_list.length; i++){
                    var classes = product_list[i].getAttribute('class')
                    var first_class = classes.split(' ')
                    product_body.push(first_class[0]) ;
                }
                function removeDuplicate(data){
                    return data.filter((value, index)=> data.indexOf(value) === index);
                }
                product_body =  JSON.stringify(removeDuplicate(product_body))

                // Order total section.
                var order_total_arr = [];
                var order_total = $('.cz_order_total ul').children();
                for(var i = 0; i< order_total.length; i++){
                    var classes = order_total[i].getAttribute('class')
                    var first_class = classes.split(' ')
                    order_total_arr[i] = first_class[0];
                }
                order_total_arr =  JSON.stringify(order_total_arr);


                var form_data = new  FormData();
                form_data.append('customizable_order_data', order_data);
                form_data.append('customizable_address', address);
                form_data.append('customizable_seller', seller);
                form_data.append('cz_header_css', customize_form);
                form_data.append('cz_order_total', order_total_arr);
                form_data.append('cz_product_header', product_header_val);
                form_data.append('cz_product_body', product_body);
                form_data.append('_ajax_nonce', woo_invoice_customize_obj.nonce);
                form_data.append('action', 'woo_invoice_customize_obj');
                $.ajax(
                    {
                        url: woo_invoice_customize_obj.woo_invoice_customize_obj_url,
                        type: 'POST',
                        contentType:false,
                        processData:false,
                        dataType: 'json',
                        data: form_data,
                        success: function (response) {
                            console.log(response)
                            $('#wpifw_customizable_shipping_btn').show();
                            $('#wpifw_customizable_shipping_btn').val('Data Saved')

                            setInterval(function (){
                                $('#wpifw_customizable_shipping_btn').hide();
                            }, 5000);
                        }
                    }
                );
            }); // end ajax.



            /*****************************************************
             * Shipping Template.
             *****************************************************/
            $("#cz_shipping_height").on('change', function () {
                let val = $(this).val()
                $('<style>#customizable_shipping_content{height:'+val+'px }</style>').appendTo('#customizable_shipping_content');
            });
            $("#cz_shipping_border_width").on('change', function () {
                let val = $(this).val()
                $('<style>#customizable_shipping_content{border-width:'+val+'px; }</style>').appendTo('#customizable_shipping_content');
            });
            $("#cz_shipping_border_color").on('change', function () {
                let val = $(this).val()
                $('<style>#customizable_shipping_content{border-color:'+val+'; }</style>').appendTo('#customizable_shipping_content');
            });

            $("#cz_shipping_border").on('change', function () {
                let val = $(this).val()
                $('<style>#customizable_shipping_content{border-style:'+val+'; }</style>').appendTo('#customizable_shipping_content');
            });

            $("#cz_shipping_background_color").on('change', function () {
                let val = $(this).val()
                $('<style>#customizable_shipping_content{background:'+val+' }</style>').appendTo('#customizable_shipping_content');
            });
            /*****************************************************
             * Shipping Header style
             *****************************************************/
            $("#cz_shipping_heading_font_size").on('change', function () {
                let val = $(this).val()
                $('<style>#customizable_shipping_content ul li div:first-child{font-size:'+val+'px }</style>').appendTo('#customizable_shipping_content');
            });

            $("#cz_shipping_heading_color").on('change', function () {
                let val = $(this).val()
                $('<style>#customizable_shipping_content ul li div:first-child{color:'+val+' }</style>').appendTo('#customizable_shipping_content');
            });

            $("#cz_shipping_heading_font_weight").on('change', function () {
                let val = $(this).val()
                $('<style>#customizable_shipping_content ul li div:first-child{font-weight:'+val+' }</style>').appendTo('#customizable_shipping_content');
            });

            // Body style.
            $("#cz_shipping_body_font_size").on('change', function () {
                let val = $(this).val()
                $('<style>#customizable_shipping_content ul li div:last-child, #customizable_shipping_content ul li div:last-child table tbody tr td{font-size:'+val+'px }</style>').appendTo('#customizable_shipping_content');
            });
            $("#cz_shipping_body_color").on('change', function () {
                let val = $(this).val()
                $('<style>#customizable_shipping_content ul li div:last-child, #customizable_shipping_content ul li div:last-child table tbody tr td{color:'+val+' }</style>').appendTo('#customizable_shipping_content');
            });

            $("#cz_shipping_body_font_weight").on('change', function () {
                let val = $(this).val()
                $('<style>#customizable_shipping_content ul li div:last-child, #customizable_shipping_content ul li div:last-child table tbody tr td{font-weight:'+val+' }</style>').appendTo('#customizable_shipping_content');
            });

            /*****************************************************
             * Shipping Template order section
             *****************************************************/
            $("#cz_shipping_display_order_data").on('change', function () {
                var val = '';
                if($(this).is(':checked')) {
                    val = 'block'
                }else{
                    val = 'none'
                }
                $('<style>#customizable_shipping_content ul li#customizable_order_data{display:'+val+' }</style>').appendTo('#customizable_shipping_content');
            });

        } );
    })(jQuery);

</script>