<?php
$cz_shipping_header_css = get_option('cz_shipping_header_css');
if( !empty($cz_shipping_header_css['cz_shipping_header_css'])) {
    $cz_shipping_header_css = explode('&',$cz_shipping_header_css['cz_shipping_header_css'] );
    $css =[];
    foreach ($cz_shipping_header_css as $key => $value) {
        $arr = explode('=', $value);
        $css[$arr[0]] = $arr[1];
    }
}
?>

<style>
    /*****************************************************
    * Shipping Template
     *****************************************************/
    #customizable_shipping_content {
        height: <?php echo (isset($css['cz_shipping_height']))? $css['cz_shipping_height'].'px' :'270px';?>;
        border-width: <?php echo (isset($css['cz_shipping_border_width']))? $css['cz_shipping_border_width'].'px' :'1px';?>;
        border-color: <?php echo (isset($css['cz_shipping_border_color']))? str_replace('%23', '#', $css['cz_shipping_border_color']) :'black';?>;
        border-style: <?php echo (isset($css['cz_shipping_border']))? $css['cz_shipping_border'] :'dashed';?>;
        position: relative;
        cursor: move;
        background: <?php echo (isset($css['cz_shipping_background_color']))? str_replace('%23', '#', $css['cz_shipping_background_color']) :'white';?> ;
    }
    #customizable_shipping_content ul li{
        float: left;
        padding: 0 10px 0 10px;
    }
    #customizable_shipping_content ul li div:first-child{
        font-weight: <?php echo (isset($css['cz_shipping_heading_font_weight']))? $css['cz_shipping_heading_font_weight'] :'bold';?>;
        font-size: <?php echo (isset($css['cz_shipping_heading_font_size']))? $css['cz_shipping_heading_font_size'].'px' :'15px';?>;
        color: <?php echo (isset($css['cz_shipping_heading_color']))? str_replace('%23', '#', $css['cz_shipping_heading_color']) :'black';?>;
    }
    #customizable_shipping_content ul li div:last-child,
    #customizable_shipping_content ul li div:last-child table tbody tr td{
        font-size: <?php echo (isset($css['cz_shipping_body_font_size']))? $css['cz_shipping_body_font_size'].'px' :'15px';?>;
        font-weight: <?php echo (isset($css['cz_shipping_body_font_weight']))? $css['cz_shipping_body_font_weight'] :'bold';?>;
        color: <?php echo (isset($css['cz_shipping_body_font_size']))? str_replace('%23', '#', $css['cz_shipping_body_color']) :'black';?>;
    }

    #customizable_shipping_content ul li#customizable_order_data{
        display: <?php echo (isset($css['cz_shipping_display_order_data']))? $css['cz_shipping_display_order_data'] :'block';?>;
    }

    #customizable_seller div img.logo{
        height: 100px;
        width: 100px;
        display: block;
    }

    /*****************************************************
    * product section.
     *****************************************************/
    /*#myTable {*/
    /*    padding:15px;*/
    /*    border: 0;*/
    /*    width: 0;*/
    /*    margin: 0 0 0 50px;*/
    /*    text-align: left;*/
    /*}*/

    /*tbody tr {*/
    /*    background: #ddffff;*/
    /*}*/

    /*.dragHandle {*/
    /*    background-image: url("http://akottr.github.io/img/handle.png");*/
    /*    background-repeat: repeat-x;*/
    /*    height: 18px;*/
    /*    margin: 0 1px;*/
    /*    cursor: move;*/
    /*}*/

    /*
 * dragtable
 *
 * @Version 2.0.0
 *
 * default css
 *
 */
    /*##### the dragtable stuff #####*/
    /*.dragtable-sortable {*/
    /*    list-style-type: none; margin: 0; padding: 0; -moz-user-select: none;*/
    /*}*/
    /*.dragtable-sortable li {*/
    /*    margin: 0; padding: 0; float: left; font-size: 1em; background: white;*/
    /*}*/

    /*.dragtable-sortable th, .dragtable-sortable td{*/
    /*    border-left: 0px;*/
    /*}*/

    /*.dragtable-sortable li:first-child th, .dragtable-sortable li:first-child td {*/
    /*    border-left: 1px solid #CCC;*/
    /*}*/

    /*.ui-sortable-helper {*/
    /*    opacity: 0.7;filter: alpha(opacity=70);*/
    /*}*/
    /*.ui-sortable-placeholder {*/
    /*    -moz-box-shadow: 4px 5px 4px #C6C6C6 inset;*/
    /*    -webkit-box-shadow: 4px 5px 4px #C6C6C6 inset;*/
    /*    box-shadow: 4px 5px 4px #C6C6C6 inset;*/
    /*    border-bottom: 1px solid #CCCCCC;*/
    /*    border-top: 1px solid #CCCCCC;*/
    /*    visibility: visible !important;*/
    /*    background: #EFEFEF !important;*/
    /*    visibility: visible !important;*/
    /*}*/
    /*.ui-sortable-placeholder * {*/
    /*    opacity: 0.0; visibility: hidden;*/
    /*}*/

    /*****************************************************
    * Order Total section.
     *****************************************************/
    .cz_order_total{
        margin-top: 50px;
        display: block;
        float: right;
        width: 320px;
        border-bottom: 2px solid black;
        cursor: move;
    }

    .cz_order_total_item {
        /*display: block;*/
        /*width: 100%;*/
    }

    .cz_order_total_key{
        display: inline;
        font-weight: bold;
        float: left;
    }
    .cz_order_total_val{
        display: inline;
    }


</style>